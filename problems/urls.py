from django.urls import path, include

from . import views

urlpatterns = [
    # The name variable here refers to the views.py functions accordingly
    path('pHome', views.pHome, name='pHome'),
    path('pUpload', views.pUpload, name='pUpload'),
    # Here we take the id of the problem and direct it to the detail page
    path('<int:problem_id>', views.pDetail, name='pDetail'),
    path('<int:problem_id>/upvote', views.upvote, name='upvote'),
]
