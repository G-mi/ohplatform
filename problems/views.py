from django.shortcuts import render, redirect, get_object_or_404
# This code allows us to later add the @login_required and only allow users that are logged in to make problem posts
from django.contrib.auth.decorators import login_required
from .models import Problem
from django.utils import timezone

@login_required(login_url="/accounts/signup")
def pUpload(request):
    if request.method == "POST":
        if request.POST['title'] and request.POST['body'] and request.POST['url'] and request.FILES['icon'] and request.FILES['image']:
            problem = Problem()
            problem.title = request.POST['title']
            problem.body = request.POST['body']
            if request.POST['url'].startswith('http') or request.POST['url'].startswith('https'):
                problem.url = request.POST['url']
            else:
                problem.url = 'http://' + request.POST['url']
            problem.icon = request.FILES['icon']
            problem.image = request.FILES['image']
            problem.pub_date = timezone.datetime.now()
            # Assigning the post to the user
            problem.user = request.user
            # This .save() is what saves it to the database
            problem.save()
            return redirect('/problems/' + str(problem.id))
        else:
            return render(request, 'problems/uploadProblem.html', {'error': 'All field are required'})
    else:
        return render(request, 'problems/uploadProblem.html')


def pHome(request):
    # This line take all the objects/solutions from the database and adds them to this variable
    problems = Problem.objects
    return render(request, 'problems/homeProblems.html', {'problems':problems})


def pDetail(request, problem_id):
    problem = get_object_or_404(Problem, pk=problem_id)
    return render(request, 'problems/detailProblem.html', {'problem':problem})

@login_required(login_url="/accounts/signup")
def upvote(request, problem_id):
    if request.method == "POST":
        problem = problem = get_object_or_404(Problem, pk=problem_id)
        problem.votes_total += 1
        # Do not forget to save changes to the database.
        problem.save()
        # After saving return to the same page. The problem that the user just upvoted.
        return redirect('/problems/' + str(problem.id))
