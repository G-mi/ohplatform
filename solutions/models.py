from django.db import models
from django.contrib.auth.models import User

class Solution(models.Model):
    title = models.CharField(max_length=200 )
    pub_date = models.DateTimeField()
    body = models.TextField()
    url = models.TextField()
    image = models.ImageField(upload_to='images/')
    icon = models.ImageField(upload_to='images/')
    votes_total = models.IntegerField(default=1)

    # User assignment of the class Problem
    user = models.ForeignKey(User, on_delete=models.RESTRICT)
    #image_width = models.IntegerField()
    #image_height = models.IntegerField()

    def summary(self):
        return self.body[:100]

    def __str__(self):
        return self.title

    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e %Y')