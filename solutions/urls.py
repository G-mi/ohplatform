from django.urls import path, include

from . import views

urlpatterns = [
    # The name variable here refers to the views.py functions accordingly
    path('sHome', views.sHome, name='sHome'),
    path('sUpload', views.sUpload, name='sUpload'),
    # Here we take the id of the problem and direct it to the detail page
    path('<int:solution_id>', views.sDetail, name='sDetail'),
    path('<int:solution_id>/upvote', views.upvote, name='upvote'),
]
