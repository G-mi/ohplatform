from django.shortcuts import render, redirect, get_object_or_404
# This code allows us to later add the @login_required and only allow users that are logged in to make solution posts
from django.contrib.auth.decorators import login_required
from .models import Solution
from django.utils import timezone

# Create your views here.

@login_required(login_url="/accounts/signup")
def sUpload(request):
    if request.method == "POST":
        # TO DO
        # implement the if statements for when the user does not provide picture
        if request.POST['title'] and request.POST['body'] and request.POST['url'] and request.FILES['icon'] and request.FILES['image']:
            solution = Solution()
            solution.title = request.POST['title']
            solution.body = request.POST['body']
            if request.POST['url'].startswith('http') or request.POST['url'].startswith('https'):
                solution.url = request.POST['url']
            else:
                solution.url = 'http://' + request.POST['url']
            solution.icon = request.FILES['icon']
            solution.image = request.FILES['image']
            solution.pub_date = timezone.datetime.now()
            # Assigning the post to the user
            solution.user = request.user
            # This .save() is what saves it to the database
            solution.save()
            return redirect('/solutions/' + str(solution.id))
        else:
            return render(request, 'solutions/uploadSolution.html', {'error': ' '})
    else:
        return render(request, 'solutions/uploadSolution.html')


def sDetail(request, solution_id):
    solution = get_object_or_404(Solution, pk=solution_id)
    return render(request, 'solutions/detailSolution.html', {'solution':solution})

def sHome(request):
    # This line take all the objects/solutions from the database and adds them to this variable
    solutions = Solution.objects
    return render(request, 'solutions/homeSolutions.html', {'solutions':solutions})

@login_required(login_url="/accounts/signup")
def upvote(request, solution_id):
    if request.method == "POST":
        solution = solution = get_object_or_404(Solution, pk=solution_id)
        solution.votes_total += 1
        # Do not forget to save changes to the database.
        solution.save()
        # After saving return to the same page. The solution that the user just upvoted.
        return redirect('/solutions/' + str(solution.id))
